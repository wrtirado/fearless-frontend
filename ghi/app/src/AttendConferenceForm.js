import React, { useEffect, useState } from 'react'
import { FetchWrapper } from './fetch-wrapper'

let spinnerClasses = 'd-flex justify-content-center mb-3'
let dropdownClasses = 'form-select d-none'
let formClasses = ''
let successMessage = 'alert alert-success d-none mb-0'

const API = new FetchWrapper('http://localhost:8001/')

function AttendConferenceForm(props) {
    const [conferences, setConferences] = useState(props.conferences)
    const [conference, setConference] = useState('')
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [formSubmitted, setFormSubmitted] = useState(false)

    if (conferences.length > 0) {
        spinnerClasses = 'd-none'
        dropdownClasses = 'for-select'
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}

        data.email = email
        data.name = name
        data.conference = conference

        const newAttendee = await API.post('api/attendees/', data)
        setName('')
        setEmail('')
        setConference('')
        setFormSubmitted(prev => true)
        setFormSubmitted(true)
    }
    
    if (formSubmitted) {
        formClasses = 'd-none'
        successMessage = 'alert alert-success mb-0'
    }
    
    const handleConferenceChange = (event) => {
        const value = event.target.value
        setConference(value)
    }

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handleEmailChange = (event) => {
        const value = event.target.value
        setEmail(value)
    }


    return (
        <>
        <div className="my-5 container">
            <div className="row">
                <div className="col col-sm-auto">
                <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" />
                </div>
                <div className="col">
                <div className="card shadow">
                    <div className="card-body">
                    <form onSubmit={handleSubmit} className={formClasses} id="create-attendee-form">
                        <h1 className="card-title">It's Conference Time!</h1>
                        <p className="mb-3">
                        Please choose which conference
                        you'd like to attend.
                        </p>
                        <div className={spinnerClasses} id="loading-conference-spinner">
                        <div className="spinner-grow text-secondary" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                        </div>
                        <div className="mb-3">
                        <select onChange={handleConferenceChange} value={conference} name="conference" id="conference" className={dropdownClasses} required>
                            <option value="">Choose a conference</option>
                            {conferences.map(conference => {
                                return (
                                    <option key={conference.id} value={conference.href}>
                                        {conference.name}
                                    </option>
                                )
                            })}
                        </select>
                        </div>
                        <p className="mb-3">
                        Now, tell us about yourself.
                        </p>
                        <div className="row">
                        <div className="col">
                            <div className="form-floating mb-3">
                            <input onChange={handleNameChange} required placeholder="Your full name" type="text" id="name" value={name} name="name" className="form-control" />
                            <label htmlFor="name">Your full name</label>
                            </div>
                        </div>
                        <div className="col">
                            <div className="form-floating mb-3">
                            <input onChange={handleEmailChange} required placeholder="Your email address" type="email" id="email" value={email} name="email" className="form-control" />
                            <label htmlFor="email">Your email address</label>
                            </div>
                        </div>
                        </div>
                        <button className="btn btn-lg btn-primary">I'm going!</button>
                    </form>
                    <div className={successMessage} id="success-message">
                        Congratulations! You're all signed up!
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default AttendConferenceForm
