import React, { useEffect, useState } from 'react'
import { FetchWrapper } from './fetch-wrapper'

function ConferenceForm() {
    const [locations, setLocations] = useState([])
    const [name, setName] = useState('')
    const [startDate, setStartDate] = useState('')
    const [endDate, setEndDate] = useState('')
    const [description, setDescription] = useState('')
    const [maxPresentations, setMaxPresentations] = useState('')
    const [maxAttendees, setMaxAttendees] = useState('')
    const [location, setLocation] = useState('')

    const API = new FetchWrapper('http://localhost:8000/')

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}

        data.name = name
        data.starts = startDate
        data.ends = endDate
        data.description = description
        data.max_presentations = maxPresentations
        data.max_attendees = maxAttendees
        data.location = location

        const newConference = await API.post('api/conferences/', data)
        setName('')
        setStartDate('')
        setEndDate('')
        setDescription('')
        setMaxPresentations('')
        setMaxAttendees('')
        setLocation('')
    }

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handleStartDateChange = (event) => {
        const value = event.target.value
        setStartDate(value)
    }

    const handleEndDateChange = (event) => {
        const value = event.target.value
        setEndDate(value)
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value
        setDescription(value)
    }

    const handleMaxPresChange = (event) => {
        const value = event.target.value
        setMaxPresentations(value)
    }

    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value
        setMaxAttendees(value)
    }

    const handleLocationChange = (event) => {
        const value = event.target.value
        setLocation(value)
    }

    const fetchData = async () => {
        const locationsData = await API.get('api/locations/')
        setLocations(locationsData.locations)
    }

    useEffect(() => {
        fetchData();
    }, [])

    
    return (
        <>
        <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form onSubmit={handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleNameChange} placeholder="Name" required type="text" value={name} name="name" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleStartDateChange} placeholder="Starts" required type="date" value={startDate} name="starts" id="starts" className="form-control" />
                                <label htmlFor="starts">Starts</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleEndDateChange} placeholder="Ends" required type="date" value={endDate} name="ends" id="ends" className="form-control" />
                                <label htmlFor="ends">Ends</label>
                            </div>
                            <div className="mb-3">
                                <p>Description</p>
                                <textarea onChange={handleDescriptionChange} required value={description} name="description" id="description" className="form-control"></textarea>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleMaxPresChange} placeholder="Maximum presentations" required type="number" value={maxPresentations} name="max_presentations" id="max-presentations" className="form-control" />
                                <label htmlFor="max-presentations">Max Presentations</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleMaxAttendeesChange} placeholder="Max attendees" required type="number" value={maxAttendees} name="max_attendees" id="max-attendees" className="form-control" />
                                <label htmlFor="max-attendees">Max attendees</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleLocationChange} required value={location} name="location" id="location" className="form-select">
                                    <option value="">Choose a location</option>
                                    {locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>
                                                {location.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        {/* <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new location</h1>
                <form onSubmit={handleSubmit} id="create-location-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleRoomCountChange} value={roomCount} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" />
                    <label htmlFor="room_count">Room count</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleCityChange} value={city} placeholder="City" required type="text" name="city" id="city" className="form-control" />
                    <label htmlFor="city">City</label>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleStateChange} value={state} required name="state" id="state" className="form-select">
                        <option key="empty" value="">
                            Choose a state
                        </option>
                        {states.map(state => {
                            return (
                                <option key={state.abbreviation} value={state.abbreviation}>
                                    {state.name}
                                </option>
                                );
                            })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div> */}
        </>
    )
}

export default ConferenceForm
