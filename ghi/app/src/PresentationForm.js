import React, { useEffect, useState } from 'react'
import { FetchWrapper } from './fetch-wrapper'

const API = new FetchWrapper('http://localhost:8000/')

function PresentationForm(props) {
    // const [conferences, setConferences] = useState(props.conferences)
    // const [conference, setConference] = useState('')
    // const [email, setEmail] = useState('')
    // const [formSubmitted, setFormSubmitted] = useState(false)
    
    const [conferences, setConferences] = useState(props.conferences)
    const [presentorName, setPresentorName] = useState('')
    const [presentorEmail, setPresentorEmail] = useState('')
    const [companyName, setCompanyName] = useState('')
    const [title, setTitle] = useState('')
    const [synopsis, setSynopsis] = useState('')
    const [conferenceID, setConferenceID] = useState('')
    
    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
    
        data.presenter_name = presentorName
        data.presenter_email = presentorEmail
        data.company_name = companyName
        data.title = title
        data.synopsis = synopsis

        const newPresentation = await API.post(`api/conferences/${conferenceID}/presentations/`, data)
        setPresentorName('')
        setPresentorEmail('')
        setCompanyName('')
        setTitle('')
        setSynopsis('')
        setConferenceID('')
    }

    const handlePresNameChange = (event) => {
        const value = event.target.value
        setPresentorName(value)
    }

    const handlePresEmailChange = (event) => {
        const value = event.target.value
        setPresentorEmail(value)
    }

    const handleCompanyNameChange = (event) => {
        const value = event.target.value
        setCompanyName(value)
    }

    const handleTitleChange = (event) => {
        const value = event.target.value
        setTitle(value)
    }

    const handleSynopsisChange = (event) => {
        const value = event.target.value
        setSynopsis(value)
    }

    const handleConferenceChange = (event) => {
        const value = event.target.value
        setConferenceID(value)
    }
            
    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form onSubmit={handleSubmit} id="create-presentation-form">
                            <div className="form-floating mb-3">
                                <input onChange={handlePresNameChange} placeholder="Presentor name" required type="text" value={presentorName} name="presenter_name" id="name" className="form-control" />
                                <label htmlFor="name">Presentor name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handlePresEmailChange} placeholder="Presentor email" required type="email" value={presentorEmail} name="presenter_email" id="email" className="form-control" />
                                <label htmlFor="email">Presentor email</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleCompanyNameChange} placeholder="Company name" type="text" value={companyName} name="company_name" id="company-name" className="form-control" />
                                <label htmlFor="company-name">Company name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleTitleChange} placeholder="Title" required type="text" value={title} name="title" id="title" className="form-control" />
                                <label htmlFor="title">Title</label>
                            </div>
                            <div className="mb-3">
                                <p>Synopsis</p>
                                <textarea onChange={handleSynopsisChange} required value={synopsis} name="synopsis" id="synopsis" className="form-control"></textarea>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleConferenceChange} required value={conferenceID} name="conference" id="conference" className="form-select">
                                    <option value="">Choose a conference</option>
                                    {conferences.map(conference => {
                                        return (
                                            <option key={conference.id} value={conference.id}>
                                                {conference.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default PresentationForm
